

def read_vars() -> str:
    print('paste the variables here, one per line.\nwhen done, add an empty new line.')

    str_vars = ''
    while (str_input := input('')):
        str_vars += str_input + '\n'

    return str_vars


def load_vars(str_vars: str) -> tuple[list[tuple[str, str]], int]:
    vars_list: list[tuple[str, str]] = []
    max_padding = 0
    for l in str_vars.split('\n'):
        if not l:
            continue

        try:
            var_name, var_type = map(str.strip, l.split(':'))
        except ValueError:
            print(f'ERROR: Could not split line: "{l}"')
            continue

        vars_list.append((var_type, var_name))
        max_padding = max(len(var_name), max_padding)

    return vars_list, max_padding


def print_vars(vars: list[tuple[str, str]], max_padding: int = 0):
    str_sep = '/'
    print(f'{" DONE ":{str_sep}^60}\n')

    sorted_vars = sorted(sorted(vars, key=lambda t: t[1]), key=lambda t: t[0])
    for vtype, vname in sorted_vars:
        print(f'  {vname:<{max_padding+1}}: {vtype}')

    print('\n' + str_sep * 60)


if __name__ == '__main__':
    vars = read_vars()
    split_vars, padding = load_vars(vars)
    print_vars(split_vars, padding)
